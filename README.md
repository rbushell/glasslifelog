Log your life with Google Glass  

Say "ok glass, record my life", and glass will take a picture of whatever you are looking at every 60 seconds.  Images are then uploaded to the web for your amusement.  

I've made some attempts to avoid draining the device's battery, but I expect battery life will still suck. 