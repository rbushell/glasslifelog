package au.com.mrtwo.lifelog;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.RemoteViews;

import com.google.android.glass.timeline.LiveCard;
import com.google.android.glass.timeline.TimelineManager;

public class LifeLogService extends Service {
	private static final long HEARTBEAT_MILLIS = 60000;

	private LiveCard mLiveCard = null;

	public class LocalBinder extends Binder {
		public LifeLogService getService() {
			return LifeLogService.this;
		}
	}

	private final IBinder mBinder = new LocalBinder();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		onServiceStart();
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onDestroy() {
		onServiceStop();
		super.onDestroy();
	}

	private boolean onServiceStart() {
		Log.d("onServiceStart() called.");

		// Publish live card...
		publishCard(this);
		setAlarms(this);

		return true;
	}

	private boolean onServiceStop() {
		Log.d("onServiceStop() called.");

		unpublishCard(this);
		cancelAlarms(this);

		return true;
	}

	private void publishCard(Context context) {
		Log.d("publishCard() called.");

		if (mLiveCard == null) {
			TimelineManager tm = TimelineManager.from(context);
			mLiveCard = tm.createLiveCard("lifelog");
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.card_lifelog);
			mLiveCard.setViews(remoteViews);

			Intent intent = new Intent(context, MenuActivity.class);
			mLiveCard.setAction(PendingIntent
					.getActivity(context, 0, intent, 0));
			mLiveCard.publish(LiveCard.PublishMode.REVEAL);
		}
	}

	private void unpublishCard(Context context) {
		Log.d("unpublishCard() called.");
		if (mLiveCard != null) {
			mLiveCard.unpublish();
			mLiveCard = null;
		}
	}

	public void setAlarms(Context context) {

		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, PictureAlarm.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
		am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
				HEARTBEAT_MILLIS, pi);

		// We need 2 separate alarms so that a long batch of image uploads
		// doesn't stop us taking pics
		i = new Intent(context, FlushAlarm.class);
		pi = PendingIntent.getBroadcast(context, 0, i, 0);
		am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 10000,  // 10 seconds for the camera to work
				HEARTBEAT_MILLIS, pi);

	}

	public void cancelAlarms(Context context) {
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		Intent intent = new Intent(context, PictureAlarm.class);
		PendingIntent sender = PendingIntent
				.getBroadcast(context, 0, intent, 0);
		alarmManager.cancel(sender);

		intent = new Intent(context, PictureAlarm.class);
		sender = PendingIntent.getBroadcast(context, 0, intent, 0);
		alarmManager.cancel(sender);
	}

}
