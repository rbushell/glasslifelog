package au.com.mrtwo.lifelog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.microedition.khronos.opengles.GL10;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.os.IBinder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.format.Time;

// Background task to take a picture
// This exists as a separate service so that it outlives the
// alarm that triggers it and our camera doesn't die
public class PictureService extends Service {
	Camera mCamera;
	Intent currentIntent; // Tracks the wakelock from WakefulBroadcastReceiver

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("Picture service is go!");
		currentIntent = intent;

		try {
			takePicture();
		} catch (Exception e) {
			Log.e("Failed to take a picture.", e);
			WakefulBroadcastReceiver.completeWakefulIntent(currentIntent);
		}

		return START_NOT_STICKY;
	}

	
	private void takePicture() throws IOException {
		Log.d("takePicture() called.");

		if (mCamera == null) {
			mCamera = Camera.open();
			int texture = createTexture();
			SurfaceTexture ts = new SurfaceTexture(texture);

			mCamera.setPreviewTexture(ts);

		}
		mCamera.startPreview();
		mCamera.takePicture(null, null, mPictureCB);

		Log.d("takePicture() done");

	}

	static private int createTexture() {
		int[] texture = new int[1];

		GLES20.glGenTextures(1, texture, 0);
		GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, texture[0]);
		GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
		GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

		return texture[0];
	}

	private PictureCallback mPictureCB = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			Log.d(String.format("Picture taken.  Got %d bytes", data.length));
			queuePicture(PictureService.this, data);

			mCamera.release();
			mCamera = null;

			WakefulBroadcastReceiver.completeWakefulIntent(currentIntent);
		}
	};

	protected void queuePicture(Context context, byte[] data) {
		Time t = new Time();
		t.setToNow();
		String filename = t.format("ll_%Y%m%dT%H%M%S.jpg");
		File f = new File(this.getFilesDir(), filename);

		FileOutputStream out = null;
		try {
			out = new FileOutputStream(f);
			out.write(data);
			Log.d(String.format("queued %s", f.toString()));

		} catch (FileNotFoundException e) {
			// Whatever, nothing we can do.
			Log.w(String.format("Failed to write %s", f));
		} catch (IOException e) {
			Log.w(String.format("Failed to write %s", f));
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (Exception e) {
				// whatever
			}
		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}