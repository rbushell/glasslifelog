package au.com.mrtwo.lifelog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class PictureAlarm extends WakefulBroadcastReceiver 
{    
	
	// Called periodically by the AlarmManager service
	// Wake up & take a picture.
    @SuppressLint("Wakelock")
	@Override
	
    public void onReceive(Context context, Intent intent) 
    {
   	 	Log.d("Picture time!");
        // Start up the picture service & take a pic
        // We can't do it here because we have only a
        // limited lifetime and the Camera goes away 
        // before the pic is complete.
   	 	Intent picIntent = new Intent(context, PictureService.class);
        startWakefulService(context, picIntent);
    }
}