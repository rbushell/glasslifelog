package au.com.mrtwo.lifelog;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.content.WakefulBroadcastReceiver;

// Background task to take a picture
// This exists as a separate service so that it outlives the
// alarm that triggers it and our camera doesn't die
public class FlushService extends Service {
	
	private class DoNetworkStuffInBackground extends AsyncTask<Intent, Void, String> {

		@Override
		protected String doInBackground(Intent... intents) {
			try {
				sendAllFiles();
			} finally {
				WakefulBroadcastReceiver.completeWakefulIntent(intents[0]);
				isActive = false;
			}
			return "ok";
		}
		
	}
	
	boolean isActive = false;

	private static final String SERVER_URL = "http://logger.rick.do/upload";
    
	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("Flush service is go!!!!!!!");

		if ( !isActive )
		{
			// Only allow one outstanding request
			isActive = true;
			new DoNetworkStuffInBackground().execute(intent);
		}
		
		return START_NOT_STICKY;
	}

	private void sendAllFiles() {

		File[] files = getCachedFiles();
		do {
			for (int a = 0; a < files.length && flushFile(files[a]); a++)
				;
			files = getCachedFiles();	 // Grab any files that have been saved since we started
		} while ( files.length > 0);

	}

	protected boolean flushFile(File file) {
		Log.d(String.format("Flushing %s", file.toString()));
		HttpClient client = new DefaultHttpClient();

		HttpPost post = new HttpPost(SERVER_URL);

		MultipartEntityBuilder multipartEntity = MultipartEntityBuilder
				.create();
		multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		multipartEntity.addPart("image", new FileBody(file));
		post.setEntity(multipartEntity.build());
		HttpResponse response;
		boolean ret = false;
		try {
			response = client.execute(post);
			HttpEntity entity = response.getEntity();
			entity.consumeContent();
			client.getConnectionManager().shutdown();
			file.delete();

			ret = true;
		} catch (ClientProtocolException e) {
			// try again later
		} catch (IOException e) {
			// try again later
		}
		Log.d(String.format(ret ? "Flushed %s" : "Failed to flush %s",
				file.toString()));
		return ret;
	}

	protected File[] getCachedFiles() {
		File cache_dir = getFilesDir();
		File[] result = cache_dir.listFiles();
		Arrays.sort(result);
		return result;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}



}