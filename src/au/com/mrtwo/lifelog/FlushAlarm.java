package au.com.mrtwo.lifelog;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class FlushAlarm extends WakefulBroadcastReceiver 
{    

	
	@Override
     public void onReceive(Context context, Intent intent) 
     {   
    	 Log.d("Uploading images");
         
    	 // Flush any cached image files in the background
    	 Intent flushIntent = new Intent(context, FlushService.class);
         startWakefulService(context, flushIntent);
     }
}