
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path')
  , fs = require('fs')
  , util = require('util')
  , sprintf = require('sprintf')
  , gm = require('gm')

var app = express();
var IMAGES_DIR='./upload';
var THUMBNAIL_DIR='./thumbs';

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser({uploadDir:IMAGES_DIR}));
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
  app.use('/upload', express.static(path.join(__dirname, 'upload')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);
app.post('/upload', function(req, res) {
  console.log(req.files);
  if ( req.files.image ) {
	// todo:  big security hole
	fs.rename ( req.files.image.path, IMAGES_DIR + "/" + req.files.image.name, function () {
  		res.write('OK');
  		res.end();
	});
  } else {
	res.write ( "huh?" );
	res.end();
  }
});

app.get('/view/:date', function(req,res) {
  var date = req.params.date;
  console.log ( date );
  var components = date.match(/^(20\d\d)-?(\d\d?)-?(\d\d?)/);
  console.log ( components );
  if ( components ) {
	var today = new Date(components[1], components[2], components[3]);
	var tomorrow = adjustDate(today,1);
	var yesterday = adjustDate(today,-1);
	console.log('Displaying images for ' + date );
  	getImagesForDate ( date, res, function (res, images) {
  		res.render('view', { title: 'Express', 'date':today, 'images': images.sort(), next: formatDateUrl(tomorrow), prev: formatDateUrl(yesterday) });
  	});
  } else {
	res.write('huh?');
	res.end();
  }
});

app.get('/thumbnail/:id', function(req,res) {
	var file = req.params.id;
	var thumbnail = THUMBNAIL_DIR + "/" + file;
	fs.exists(thumbnail, function(exists) {
		if ( !exists ) {
			gm(IMAGES_DIR + "/" + file)
				.resize(100,100)
				.write(thumbnail, function(err) {
					if ( !err ) {
						res.sendfile(thumbnail);
					} else {
						res.send(500,"Oops" + err.toString());
					}
				});
		} else {
			res.sendfile(thumbnail);
		}
	});
});

					
				
				
		


http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

function getImagesForDate(date, resp, callback)
{
	fs.readdir(IMAGES_DIR, function(err, files) {
		if ( err ) {
			console.log ( 'Error reading directory ' + IMAGES_DIR );
			callback ( resp, [] );
		} else {
			callback ( resp, filterFiles(files,date));
		}
	});
}

function filterFiles(file_list, filter)
{
	var results = [];
	file_list.forEach(function(entry) {
		if ( entry.indexOf(filter) >= 0) {
			results.push(entry);
		}
	});
	console.log ( results.length);
	return results;
}

function formatDateUrl(date)
{
	return sprintf("%04d%02d%02d",date.getFullYear(), date.getMonth(), date.getDate());	
}

function adjustDate (date, amount) {
    var tmpDate = new Date(date);
    tmpDate.setDate(tmpDate.getDate() + amount)
    return tmpDate;
};
